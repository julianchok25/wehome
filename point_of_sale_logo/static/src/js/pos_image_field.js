odoo.define("point_of_sale_logo.image", function (require) {
    "use strict";
    var PosBaseWidget = require('point_of_sale.chrome');
    var screens = require('point_of_sale.screens');
    var core = require('web.core');

    // let order_ini = 0;
    let cons_final = 0;

    


    var QWeb = core.qweb;
    // console.log("PosBaseWidget", PosBaseWidget)
    screens.ReceiptScreenWidget.include({
        render_receipt: function () {
            this._super(this);
            var order = this.pos.get_order()

            
            // ************ start codigo Daniel ************

            cons_final+=1;
            var ncons = cons_final;
            console.log(cons_final)

            var old = order.pos.order.name;
            var fcons = 0;
            if (ncons < 10){
                fcons = '0000'+String(ncons);
            }else if(ncons > 9 && ncons < 100){
                fcons = '000'+String(ncons);
            }else if(ncons > 99 && ncons < 1000){
                fcons = '00'+String(ncons);
            }else if(ncons > 999 && ncons < 10000){
                fcons = '0'+String(ncons);
            }else{
                fcons = String(ncons);
            }
             
            let nfact = old.substring(0,3)+fcons;

            // ************** End codigo Daniel ***********************

            this.$('.pos-receipt-container').html(QWeb.render('PosTicket',{
                    widget:this,
                    a2 : window.location.origin + '/web/image?model=pos.config&field=image&id='+this.pos.config.id,
                    fact: nfact,
                    order: order,
                    receipt: order.export_for_printing(),
                    orderlines: order.get_orderlines(),
                    paymentlines: order.get_paymentlines(),
                }));

        },
    });
    PosBaseWidget.Chrome.include({
        renderElement:function () {

            var self = this;
            console.log("self:", self) 
            
            

            if(self.pos.config){
                if(self.pos.config.image){
                    this.flag = 1
                    this.a3 = window.location.origin + '/web/image?model=pos.config&field=image&id='+self.pos.config.id;
                }
            }
            this._super(this);

            // *********** Start codigo Daniel *********** //

            if('order' in this.pos){

                var order_ini = this.pos.order.name;
                var cons_ini  = parseInt(order_ini.substring(4,9));
                cons_ini = parseInt(cons_ini);
                cons_final = cons_ini;
            }

            // ************** End Condigo Daniel ************ //
        }
    });
});
