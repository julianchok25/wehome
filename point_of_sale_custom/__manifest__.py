# -*- coding: utf-8 -*-
{
    'name': 'Point of Sale Custom',
    'author': 'John W. viloria Amaris',
    'license': 'AGPL-3',
    'category': 'Point Of Sale',
    'description': """
    Point of sale enhancements
    """,
    'depends': ['point_of_sale','point_of_sale_logo'],
    'data': [
        'views/pos_config_view.xml',
        'views/res_company_view.xml',       #REQ #11
        'report_paperformat.xml',           #REQ #23
        'report/report_pos_receipt.xml',    #REQ #23
        'views/pos_receipt.xml',            #REQ #23
    ],
    'qweb': [
        'static/src/xml/pos.xml',
    ],
    'installable': True,
    'auto_install': False,
}
