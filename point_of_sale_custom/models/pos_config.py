# -*- coding: utf-8 -*-

from odoo import api, fields, models
import logging

_logger = logging.getLogger(__name__)

class PosConfig(models.Model):
    _inherit = 'pos.config'

    # REQ #11
    def body_builder(self, pos, template):
        try:
            body = template % pos.name
        except Exception as e:
            return "<h2>Error en plantilla</h2>"
        return body

    @api.multi
    def validate_res_dian(self):
        for pos in self.search([]):
            company_id = pos.company_id.id
            company = self.env['res.company'].\
                search([('id','=',company_id)])[0]
            email = company.account_alert_email
            email_template = company.html_msg_alert_email
            msg = 'caja: %s secuencia: %s email: %s'%(pos.name, \
                pos.sequence_id.number_next_actual, email)
            _logger.critical(msg)
            if pos.res_dian_seq_end > 0:
                if pos.sequence_id.number_next_actual  >= \
                    (pos.res_dian_seq_end - pos.notify_subtracting_seq):
                    mail_values = {
                        'subject': 'URGENTE, pronto vencimiento resolucion caja: %s'%pos.name,
                        'body_html': self.body_builder(pos, email_template),
                        'email_to': company.account_alert_email,
                        #'email_from': 'ventas@wehome.com.co',
                       }
                    try:
                        create_and_send_email = self.env['mail.mail'].create(mail_values).send()
                    except Exception as e:
                        _logger.critical('ERROR ENVIANDO CORREO: %s'%e)
    # FIN REQ #11
    
    # Req #12
    res_dian_line1 = fields.Char('Line 1',size=100)
    res_dian_line2 = fields.Char('Line 2',size=100)
    res_dian_line3 = fields.Char('Line 3',size=100)
    res_dian_line4 = fields.Char('Line 4',size=100)
    res_dian_date_from = fields.Date('From')
    res_dian_date_to = fields.Date('To')
    res_dian_seq_start = fields.Integer('Start Sequence')
    res_dian_seq_end = fields.Integer('End Sequence')
    res_dian_prefix = fields.Char('Prefix')
    notify_subtracting_seq = fields.Integer('Notify Subtracting Seq')
    notify_subtracting_days = fields.Integer('Notify Subtracting Days')
    # Fin Req #12