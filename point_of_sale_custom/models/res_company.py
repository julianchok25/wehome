# -*- coding: utf-8 -*-

from odoo import fields, models

class ResCompany(models.Model):
    _inherit = "res.company"

    #REQ #11
    account_alert_email = fields.Char(string='Account Responsible')
    sale_alert_email = fields.Char(string='Sale Responsible')
    purchase_alert_email = fields.Char(string='Purchase Responsible')
    stock_alert_email = fields.Char(string='Stock Responsible')
    html_msg_alert_email = fields.Text(string='Message Template')
    #FIN REQ #11