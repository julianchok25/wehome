from odoo import api, models
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as DTF
import pytz
import logging

_logger = logging.getLogger(__name__)

#REQ #23
def str_to_datetime(strdate):
    try:
        return datetime.strptime(strdate, DTF)
    except Exception as e:
        return False

class PosOrderReceipt(models.AbstractModel):
    _name = 'report.point_of_sale_custom.pos_receipt'
    
    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('point_of_sale_custom.pos_receipt')
        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': self.env['pos.order'].search([('id','in',docids)]),
            'get_journal_amt': self._get_journal_amt,
            'utc_to_lctime': self._utc_to_lctime,
            'get_total_product_qty': self._get_total_product_qty
        }
        return report_obj.render('point_of_sale_custom.pos_receipt', docargs)

    def _get_journal_amt(self, order_id):
        data={}
        sql = """ select aj.name,absl.amount as amt from account_bank_statement as abs
                        LEFT JOIN account_bank_statement_line as absl ON abs.id = absl.statement_id
                        LEFT JOIN account_journal as aj ON aj.id = abs.journal_id
                        WHERE absl.pos_statement_id =%d"""%(order_id)
        self._cr.execute(sql)
        data = self._cr.dictfetchall()
        res = []
        change = {'name':'Change','amt': 0}
        for line in data:
            if line['amt'] < 0:
                change['amt'] = line['amt'] * -1
                continue
            res.append(line)
        res.append(change)
        return res

    def netamount(self, order_line_id):
        sql = 'select (qty*price_unit) as net_price from pos_order_line where id = %s'
        self._cr.execute(sql, (order_line_id,))
        res = self._cr.fetchone()
        return res[0]

    def discount(self, order_id):
        sql = 'select discount, price_unit, qty from pos_order_line where order_id = %s '
        self._cr.execute(sql, (order_id,))
        res = self._cr.fetchall()
        dsum = 0
        for line in res:
            if line[0] != 0:
                dsum = dsum +(line[2] * (line[0]*line[1]/100))
        return dsum

    def _utc_to_lctime(self, strdate):
        if self._context is None:
            self._context = {}
        user_tz = pytz.timezone(self._context['tz']) if 'tz' in self._context \
                                else pytz.timezone('America/Bogota')
        date = str_to_datetime(strdate)
        if date:
            today = datetime.today()
            tzoffset = user_tz.utcoffset(today)
            date = date + tzoffset
            return date.strftime('%d/%m/%Y %H:%M')
        return strdate

    def _get_total_product_qty(self, order_id):
        sql = 'select qty from pos_order_line where order_id = %s '
        self._cr.execute(sql, (order_id,))
        res = self._cr.fetchall()
        qty_sum = 0
        for line in res:
            if line[0] > 0:
                qty_sum += line[0]
        return qty_sum