# -*- coding: utf-8 -*-
{
    'name': 'Reports of Point of Sale',
    'author': 'John W. viloria Amaris',
    'license': 'AGPL-3',
    'category': 'Point Of Sale',
    'description': """
    Reports Point of sale.
    """,
    'depends': ['point_of_sale'],
    'data': [
        'wizard/pos_details.xml',
        'views/report_saledetails.xml',
    ],
    'qweb': [
    ],
    'installable': True,
    'auto_install': False,
}
