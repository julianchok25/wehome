# -*- coding: utf-8 -*-

from odoo import api, fields, models
from datetime import timedelta
from operator import itemgetter
import pytz

import logging

_logger = logging.getLogger(__name__)

class ReportSaleDetails(models.AbstractModel):

    _inherit = 'report.point_of_sale.report_saledetails'

    @api.model
    def get_sale_details(self, date_start=False, date_stop=False, configs=False):
        """ Serialise the orders of the day information

        params: date_start, date_stop string representing the datetime of order
        """
        if not configs:
            configs = self.env['pos.config'].search([])

        user_tz = pytz.timezone(self.env.context.get('tz') or self.env.user.tz or 'UTC')
        today = user_tz.localize(fields.Datetime.from_string(fields.Date.context_today(self)))
        today = today.astimezone(pytz.timezone('UTC'))
        if date_start:
            date_start = fields.Datetime.from_string(date_start)
        else:
            # start by default today 00:00:00
            date_start = today

        if date_stop:
            # set time to 23:59:59
            date_stop = fields.Datetime.from_string(date_stop)
        else:
            # stop by default today 23:59:59
            date_stop = today + timedelta(days=1, seconds=-1)

        # avoid a date_stop smaller than date_start
        date_stop = max(date_stop, date_start)

        date_start = fields.Datetime.to_string(date_start)
        date_stop = fields.Datetime.to_string(date_stop)

        orders = self.env['pos.order'].search([
            ('date_order', '>=', date_start),
            ('date_order', '<=', date_stop),
            ('state', 'in', ['paid','invoiced','done']),
            ('config_id', 'in', configs.ids)])

        user_currency = self.env.user.company_id.currency_id

        total = 0.0
        products_sold = {}
        taxes = {}
        customers = []
        category_products_qty = {}
        category_products_value = {}
        pos_qty = {}
        pos_value = {}
        for order in orders:
            if user_currency != order.pricelist_id.currency_id:
                total += order.pricelist_id.currency_id.compute(order.amount_total, user_currency)
            else:
                total += order.amount_total
            currency = order.session_id.currency_id
            if order.partner_id.id not in customers:
                customers.append(order.partner_id.id)
            pos_id = (order.config_id)
            pos_qty.setdefault(pos_id, 0.0)
            pos_qty[pos_id] += 1
            pos_value.setdefault(pos_id, 0.0)
            pos_value[pos_id] += order.amount_total

            for line in order.lines:
                cat = (line.product_id.categ_id)
                key = (line.product_id, line.price_unit, line.discount)
                products_sold.setdefault(key, 0.0)
                products_sold[key] += line.qty
                category_products_qty.setdefault(cat, 0.0)
                category_products_qty[cat] += line.qty
                category_products_value.setdefault(cat, 0.0)
                category_products_value[cat] += line.price_subtotal_incl

                if line.tax_ids_after_fiscal_position:
                    line_taxes = line.tax_ids_after_fiscal_position.compute_all(line.price_unit * (1-(line.discount or 0.0)/100.0), currency, line.qty, product=line.product_id, partner=line.order_id.partner_id or False)
                    for tax in line_taxes['taxes']:
                        taxes.setdefault(tax['id'], {'name': tax['name'], 'total':0.0})
                        taxes[tax['id']]['total'] += tax['amount']

        st_line_ids = self.env["account.bank.statement.line"].search([('pos_statement_id', 'in', orders.ids)]).ids
        if st_line_ids:
            self.env.cr.execute("""
                SELECT aj.name, sum(amount) total
                FROM account_bank_statement_line AS absl,
                     account_bank_statement AS abs,
                     account_journal AS aj 
                WHERE absl.statement_id = abs.id
                    AND abs.journal_id = aj.id 
                    AND absl.id IN %s 
                GROUP BY aj.name
            """, (tuple(st_line_ids),))
            payments = self.env.cr.dictfetchall()
        else:
            payments = []
        category_products_qty = [(key.name, value, key.id) for key, value in category_products_qty.iteritems()]
        category_products_qty.sort(key=itemgetter(1), reverse=True)
        category_products = []
        for cat_name, qty, cat_id in category_products_qty:
            for cat, value in category_products_value.iteritems():
                if cat.id == cat_id:
                    category_products.append((cat_name, qty, value))
        pos_qty = [(pos.name, qty, pos.id) for pos, qty in pos_qty.iteritems()]
        pos_qty.sort(key=itemgetter(1), reverse=True)
        pos_summary = []
        for pos_name, qty, pos_id in pos_qty:
            for pos, value in pos_value.iteritems():
                if pos.id == pos_id:
                    pos_summary.append((pos_name, qty, value))

        return {
            'currency_precision': user_currency.decimal_places,
            'total_paid': user_currency.round(total),
            'payments': payments,
            'company_name': self.env.user.company_id.name,
            'taxes': taxes.values(),
            'products': sorted([{
                'product_id': product.id,
                'product_name': product.name,
                'code': product.default_code,
                'quantity': qty,
                'price_unit': price_unit,
                'discount': discount,
                'uom': product.uom_id.name
            } for (product, price_unit, discount), qty in products_sold.items()], key=lambda l: l['product_name']),
            'category_products': category_products,
            'customer_qty': len(customers),
            'pos_summary': pos_summary
        }


class posDetails(models.TransientModel):
    _inherit = 'pos.details.wizard'

    summary = fields.Boolean('Summary')

    @api.multi
    def generate_report(self):
        data = {'summary': self.summary,'date_start': self.start_date, \
                'date_stop': self.end_date, 'config_ids': self.pos_config_ids.ids}
        return self.env['report'].get_action(
            [], 'point_of_sale.report_saledetails', data=data)