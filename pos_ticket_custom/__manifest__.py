# -*- coding: utf-8 -*-
{
    'name': 'Custom pos ticket ',
    'author': 'John W. viloria Amaris',
    'license': 'AGPL-3',
    'category': 'Point Of Sale',
    'description': """
    Logo y otros datos en el tiquete de venta
    """,
    'depends': ['point_of_sale_logo'],
    'data': [
        'data.xml'
    ],
    'qweb': [
        'static/src/xml/pos.xml',
    ],
    'installable': True,
    'auto_install': False,
}
