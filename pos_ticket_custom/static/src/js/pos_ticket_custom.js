odoo.define('pos_ticket_custom.pos_ticket_custom', function (require) {
    "use strict";

    var models = require('point_of_sale.models');
    var screens = require('point_of_sale.screens');
    var core = require('web.core');
    var QWeb = core.qweb;

    models.load_fields('res.partner', ['ref']);
    models.load_fields('res.company', ['logo_web','street']);
    
    var _super_orderline = models.Orderline.prototype;
    models.Orderline = models.Orderline.extend({
        initialize: function(attr,options){
            _super_orderline.initialize.apply(this, arguments);
            this.bcode = this.product.barcode.slice(-6);    //REQ #19
        }
    });

    return models;
});
