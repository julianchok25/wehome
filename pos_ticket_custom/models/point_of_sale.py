# -*- coding: utf-8 -*-

from odoo import api, fields, models
import re
import logging

_logger = logging.getLogger(__name__)

class pos_session(models.Model):
    _inherit = 'pos.session'

    @api.model
    def create(self, vals):
        session = super(pos_session, self).create(vals)
        pos_id = session.config_id.id
        orders = self.env['pos.order'].search(
            [('config_id','=',pos_id)], order='id desc', limit=1000)
        maxVal = 0
        if orders:
            for order in orders:
                try:
                    parts = re.findall('\d+',order.pos_reference)
                    if len(parts) > 1:
                        pos_reference = int(parts[len(parts) - 1])
                    else:
                        pos_reference = int(parts[0])
                    _logger.critical('pos_reference: %s'%pos_reference)
                except Exception as e:
                    _logger.critical('Error: %s'%e)
                    pos_reference = 0
                if pos_reference > maxVal: maxVal = pos_reference
        session.sequence_number = maxVal + 1
        _logger.critical('MaxVal: %s'%maxVal)
        return session

class posConfig(models.Model):
    _inherit = 'pos.config'

    prefix = fields.Char(related='sequence_id.prefix', string='Prefix')